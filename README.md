run on terminal
``` rails s```
then open in post man
use this
method | api | description
-------|-----|------------
GET    |http://localhost:3000/api/v1/teachers | show all
GET |http://localhost:3000/api/v1/teachers/1 | show id 1
PUT | http://localhost:3000/api/v1/teachers/1 | update id 1
PUT | http://localhost:3000/api/v1/teachers/3 | delete id 3
POST | http://localhost:3000/api/v1/teachers | create new id

